from __future__ import absolute_import

from django.http import HttpResponse

from hello import launchdarkly


def root(request):
    return HttpResponse('LaunchDarkly client initialized? ' + str(launchdarkly.client.is_initialized()))


def variation(request):
    return HttpResponse(launchdarkly.client.variation(launchdarkly.FLAG_KEY, {"key": "userKey"}, "default"))
