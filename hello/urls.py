from django.conf.urls import url

import views

urlpatterns = [
    url(r'^$', views.root, name='index'),
    url(r'^variation$', views.variation, name='variation'),
]
