import ldclient
from ldclient import Config

SDK_KEY = "YOUR_SDK_KEY_HERE"
FLAG_KEY = "YOUR_FLAG_KEY_HERE"

ldclient.set_config(Config(sdk_key=SDK_KEY))
client = ldclient.get()
