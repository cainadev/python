### LaunchDarkly Sample Django Application  ###
We've built a simple web application that demonstrates how LaunchDarkly's SDK works.  Below, you'll find the basic build procedure, but for more comprehensive instructions, you can visit your [Quickstart page](https://app.launchdarkly.com/quickstart#/).
##### Build instructions  #####
1. Install requirements by running `pip install -r requirements.txt`
2. Copy your SDK key and feature flag key from your LaunchDarkly dashboard into `hello/launchdarkly/init.py` 
3. Run `gunicorn hello.wsgi --config gunicorn.conf`